#!/usr/bin/bash
#
# This file will allow automated checking of project 3. It requires that you create a git branch with
# the unmodified project 3 starter code (BASEBRANCH), a branch with your modified code (TESTBRANCH) 
# and a directory holding test programs (TESTCASES)
#

export BASEOUTDIR=base_outputs
export TESTOUTDIR=test_outputs
export BASECASES=../project3_gt/test_progs
export TESTCASES=test_progs
export BASEBRANCH=base
export TESTBRANCH=master
export ASSEMBLY_EXT=s
export PROGRAM_EXT=c

function generate_base_outputs {
    cd /home/haolind/Desktop/project3_gt
    if [ -d $BASEOUTDIR ]; then
        echo "$0: Deleting old base outputs from $BASEOUTDIR"
        rm -r $BASEOUTDIR
        mkdir $BASEOUTDIR
    else
        mkdir $BASEOUTDIR
    fi
    # echo "$0: Building Test simv on branch [$BASEOUTDIR]"
    make clean simv > /dev/null
    echo "$0: Done."
    run_tests_base ${ASSEMBLY_EXT} ${BASEOUTDIR} assembly 
    run_tests_base ${PROGRAM_EXT} ${BASEOUTDIR} program 

}


function run_tests_base {
    cd /home/haolind/Desktop/project3_gt
    extension=$1
    output=$2
    type=$3
    echo "$0: Testing $type files"
    for tst in $BASECASES/*.$extension; do
        testname=$tst
        testname=${testname##${BASECASES}\/}
        testname=${testname%%.${extension}}
        echo "$0: Test: $testname"
        make $type SOURCE=$tst > /dev/null
        ./simv > program.out
        grep "@@@" program.out > $2/$testname.program.out
        grep "CPI" program.out > $2/$testname.cpi.out
        mv pipeline.out $2/$testname.pipeline.out
        mv writeback.out $2/$testname.writeback.out
        break
    done

}
function run_tests_test {
    cd /home/haolind/Desktop/project3
    extension=$1
    output=$2
    type=$3
    echo "$0: Testing $type files"

    for tst in $TESTCASES/*.$extension; do
        testname=$tstba
        testname=${testname##${TESTCASES}\/}
        testname=${testname%%.${extension}}
        echo "$0: Test: $testname"
        make $type SOURCE=$tst > /dev/null
        ./simv > program.out
        grep "@@@" program.out > $2/$testname.program.out
        grep "CPI" program.out > $2/$testname.cpi.out
        mv pipeline.out $2/$testname.pipeline.out
        mv writeback.out $2/$testname.writeback.out
        break
    done
}

function generate_test_outputs {
    cd /home/haolind/Desktop/project3
    # create test outputs
    # git checkout $TESTBRANCH
    if [ -d $TESTOUTDIR ]; then
        echo "$0: Deleting old test outputs from $TESTOUTDIR"
        rm -r $TESTOUTDIR
        mkdir $TESTOUTDIR
    else
        mkdir $TESTOUTDIR
    fi
    # echo "$0: Building Test simv on branch [$TESTBRANCH]"
    make clean simv > /dev/null
    echo "$0: Done."
    run_tests_test ${ASSEMBLY_EXT} ${TESTOUTDIR} assembly 
    run_tests_test ${PROGRAM_EXT} ${TESTOUTDIR} program 
}

function compare_results {
    printf "\TEST RESULTS:\n"
    # compare results
    pass_count=$((0))
    fail_count=$((0))
    total=$((0))
    for tst in $TESTOUTDIR/*.program.out; do
        testname=$tst
        testname=${testname##${TESTOUTDIR}\/}
        testname=${testname%%.program.out}
        diff $tst $BASEOUTDIR/$testname.program.out > /dev/null
        status=$? # 0 -> no difference
        if [[ "$status" -eq "0" ]]; then
            echo "$0: Test $testname PASSED"
            pass_count=$(($pass_count + 1))
        else
            echo "$0: Test $testname FAILED"
            fail_count=$(($fail_count + 1))
        fi
        echo "BASE PERF `cat $BASEOUTDIR/$testname.cpi.out`"
        echo "TEST PERF `cat $TESTOUTDIR/$testname.cpi.out`"
        echo ""
        total=$(($total + 1))
    done
    echo ""
    echo "PASSED $pass_count/$total tests ($fail_count failures)."
}

# changes=`git status --porcelain | grep -v "??"`

# if [ -n "$changes" ]; then
#     echo "Please commit/revert pending changes on current branch before running $0:"
#     echo $changes
#     exit
# fi

generate_base_outputs
generate_test_outputs
compare_results

